
from flask_wtf import Form
from wtforms import TextField, PasswordField, BooleanField
from wtforms.validators import Required
from wtforms.validators import Length, Email


class SignupForm(Form):
    first_name = TextField('First name')

    last_name = TextField('Last name')

    username = TextField('Choose username', validators=[Required()])

    password = PasswordField('Password', validators=[
               Required(),
               Length(min=6, message=(u'Please give a longer password'))])

    email = TextField('Email address', validators=[
                Required('Please provide a valid email address'),
                Length(min=6, message=(u'Email address too short')),
                Email(message=(u'That\'s not a valid email address.'))])


class SigninForm(Form):
    username = TextField('Username', validators=[
               Required(),
               Length(min=3,
                      message=(u'Your username must be a minimum of 3'))])

    password = PasswordField('Password', validators=[
               Required(),
               Length(min=5, message=(u'Please give a longer password'))])

    remember_me = BooleanField('Remember me', default=False)
