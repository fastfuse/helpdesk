
from app import db
from datetime import datetime, timedelta
from app import config


class User(db.Model):
    __tablename__ = 'users'

    id = db.Column(db.Integer, primary_key=True)
    first_name = db.Column(db.String(50))
    last_name = db.Column(db.String(50))
    username = db.Column(db.String(20), unique=True)
    password = db.Column('password', db.String(10))
    email = db.Column('email', db.String(50), unique=True, index=True)
    registered_on = db.Column('registered_on', db.DateTime)

    user_role = db.Column(db.Integer, db.ForeignKey('roles.id'))

    user_issuer = db.relationship('Ticket',
                                  backref='ticket_issuer',
                                  foreign_keys='Ticket.issuer')

    user_handler = db.relationship('Ticket',
                                   backref='ticket_handler',
                                   foreign_keys='Ticket.handler')

    def __init__(self, first_name="", last_name="", username="",
                 password="", email=""):
        self.first_name = first_name
        self.last_name = last_name
        self.username = username
        self.password = password
        self.email = email
        self.registered_on = datetime.utcnow().\
            replace(microsecond=0) + timedelta(hours=3)

    def is_authenticated(self):
        return True

    def is_active(self):
        return True

    def is_anonymous(self):
        return False

    def get_id(self):
        return unicode(self.id)

    def __repr__(self):
        return self.username


class Role(db.Model):
    __tablename__ = 'roles'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50))
    description = db.Column(db.String(200))

    user = db.relationship('User', backref='role', lazy='dynamic')

    def __init__(self, name="", description=""):
        self.name = name
        self.description = description

    def __repr__(self):
        return self.name


class Ticket(db.Model):
    __tablename__ = 'tickets'

    id = db.Column(db.Integer, primary_key=True)
    problem = db.Column(db.String(200))
    description = db.Column(db.String(500))
    priority = db.Column(db.String(50))
    status = db.Column(db.String(50))
    date = db.Column(db.DateTime)
    expected_solving_date = db.Column(db.DateTime)
    solving_date = db.Column(db.DateTime)
    issuer = db.Column(db.Integer, db.ForeignKey('users.id'))
    handler = db.Column(db.Integer, db.ForeignKey('users.id'), default=None)

    def __init__(self, problem="", description="", priority="", issuer=None):
        self.date = datetime.utcnow().replace(microsecond=0) + \
                                                             timedelta(hours=3)
        self.problem = problem
        self.description = description
        self.priority = priority
        self.status = config.STATUS[0]
        self.issuer = issuer

    def __repr__(self):
        return 'Ticket \"{}\"'.format(self.problem)
