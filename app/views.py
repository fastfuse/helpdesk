
import models
import forms
import common

from app import app, db, admin, login_manager
from flask import request, flash, url_for, redirect, render_template, session
from flask_admin.contrib.sqla import ModelView
from flask_login import login_user, logout_user, current_user,\
                            login_required
from app import config
from functools import wraps
from datetime import datetime, timedelta
from sqlalchemy import desc


# ================   Admin   ================================

class SecureModelView(ModelView):
    def is_accessible(self):
        return current_user.role.name == "Admin"


admin.add_view(SecureModelView(models.User, db.session))
admin.add_view(SecureModelView(models.Role, db.session))
admin.add_view(SecureModelView(models.Ticket, db.session))


# =====================   Login views   =====================

@app.route('/signup', methods=['GET', 'POST'])
def signup():
    if request.method == 'POST':
        form = forms.SignupForm(request.form)
        if form.validate():
            user = models.User()
            form.populate_obj(user)
            user_exist = models.User.query.filter_by(
                username=form.username.data).first()

            email_exist = models.User.query.filter_by(
                email=form.email.data).first()

            if user_exist:
                form.username.errors.append('Username already taken')

            if email_exist:
                form.email.errors.append('Email already in use')

            if user_exist or email_exist:
                return render_template('signup.html', form=form)

            else:
                user.first_name = form.first_name.data
                user.last_name = form.last_name.data
                user.password = common.hash_password(user.password)
                user.role = models.Role.query.filter_by(name='User').first()

                db.session.add(user)
                db.session.commit()

                return redirect('signin')
        else:
            return render_template('signup.html', form=form)

    return render_template('signup.html', form=forms.SignupForm())


login_manager.login_view = 'signin'


def required_roles(*roles):
    def wrapper(f):
        @wraps(f)
        def wrapped(*args, **kwargs):
            if get_current_user_role() not in roles:
                flash('Authentication error. You are not allowed '
                      'to see this page!')
                return redirect(url_for('index'))
            return f(*args, **kwargs)
        return wrapped
    return wrapper


def get_current_user_role():
    return current_user.role.name


@login_manager.user_loader
def load_user(id):
    return models.User.query.get(int(id))


@app.route('/signin', methods=['GET', 'POST'])
def signin():
    if request.method == 'POST':
        if current_user is not None and current_user.is_authenticated:
            return redirect(url_for('index'))

        form = forms.SigninForm(request.form)
        if form.validate():
            user = models.User.query.filter_by(
                username=form.username.data).first()

            if user is None:
                form.username.errors.append('Username not found')
                return render_template('signin.html', signin_form=form)

            if user.password != common.hash_password(form.password.data):
            # if user.password != form.password.data:
                form.password.errors.append('Password did not match')
                return render_template('signin.html', signin_form=form)

            login_user(user, remember=form.remember_me.data)
            session['signed'] = True
            session['user'] = {'id': user.id, 'username': user.username,
                               'role': user.role.name}

            if session.get('next'):
                next_page = session.get('next')
                session.pop('next')
                return redirect(next_page)

            else:
                return redirect(url_for('index'))
        return render_template('signin.html', signin_form=form)

    else:
        session['next'] = request.args.get('next')
        return render_template('signin.html',
                               signin_form=forms.SigninForm())


@app.route('/signout')
def signout():
    session.pop('signed')
    session.pop('user')
    logout_user()
    return redirect(url_for('index'))

# ====================================================


@app.route('/')
@app.route('/index')
@app.route('/index/<int:page>')
def index(page=1):
    if 'user' in session.keys():
        all_tickets = models.Ticket.query.order_by(desc(models.Ticket.date)).\
            paginate(page, config.TICKETS_PER_PAGE, False)

        return render_template('index.html',
                               all_tickets=all_tickets,
                               my_tickets=models.Ticket.query.
                               filter_by(issuer=current_user.id).
                               order_by(desc(models.Ticket.date)).all(),
                               priorities=config.PRIORITY,
                               signin_form=forms.SigninForm())
    else:
        return render_template('index.html', signin_form=forms.SigninForm())


@app.route('/add_ticket', methods=['GET', 'POST'])
@login_required
def add_ticket():
    user = current_user
    ticket = models.Ticket(request.form['problem'],
                           request.form['description'],
                           request.form['priority'],
                           user.id)

    if request.form['priority'] == config.PRIORITY[0]:
        ticket.expected_solving_date = datetime.utcnow().\
            replace(microsecond=0) + timedelta(days=3, hours=3)
    elif request.form['priority'] == config.PRIORITY[1]:
        ticket.expected_solving_date = datetime.utcnow().\
            replace(microsecond=0) + timedelta(days=2, hours=3)
    elif request.form['priority'] == config.PRIORITY[2]:
        ticket.expected_solving_date = datetime.utcnow().\
            replace(microsecond=0) + timedelta(days=1, hours=3)

    db.session.add(ticket)
    db.session.commit()

    return redirect(url_for('index'))


@app.route('/my_tickets/<int:id>')
@required_roles('Admin')
def my_tickets(id):
    return render_template('my_tickets.html',
                           ongoing=models.Ticket.query.
                           filter_by(handler=id,
                                     status=config.STATUS[2]).all(),
                           postponed=models.Ticket.query.
                           filter_by(handler=id,
                                     status=config.STATUS[4]).all(),
                           blocked=models.Ticket.query.
                           filter_by(handler=id,
                                     status=config.STATUS[5]).all(),
                           queued=models.Ticket.query.
                           filter_by(status=config.STATUS[0]).all(),
                           solved=models.Ticket.query.
                           filter_by(status=config.STATUS[3],
                                     handler=id).all(),
                           all_tickets=models.Ticket.query.all(),
                           status=config.STATUS[3:])


@app.route('/take_ticket/<int:id>', methods=['GET', 'POST'])
@required_roles('Admin')
def take_ticket(id):
    user = current_user
    ticket = models.Ticket.query.get(id)
    ticket.handler = user.id
    ticket.status = config.STATUS[2]

    db.session.add(ticket)
    db.session.commit()

    return redirect(url_for('my_tickets', id=user.id))


@app.route('/change_status/<int:id>/<status>', methods=['GET', 'POST'])
@required_roles('Admin')
def change_status(id, status):
    ticket = models.Ticket.query.get(id)
    ticket.status = status

    if status == config.STATUS[3]:
        ticket.solving_date = datetime.utcnow().replace(microsecond=0) +\
            timedelta(hours=3)

    db.session.add(ticket)
    db.session.commit()

    return redirect(request.referrer)


@app.route('/ticket/<int:id>')
def ticket_details(id):
    return render_template('ticket.html', ticket=models.Ticket.query.get(id))


@app.route('/test')
def test_page():
    pass


@app.route('/profile')
@login_required
def profile():
    return render_template('profile.html')
