
import md5

from app import config


def hash_password(password):
    salted_hash = password + config.SECRET_KEY.decode('utf-8')
    return md5.new(salted_hash).hexdigest()
