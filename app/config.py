
import os

DEBUG = False
CSRF_ENABLED = True
PROPAGATE_EXCEPTIONS = True
# SECRET_KEY = os.environ.get('SECRET_KEY',
#                             '\xfb\x13\xdf\xa1@i\xd6>V\xc0\xbf\x8fp'
#                             '\x16#Z\x0b\x81\xeb\x16')

SECRET_KEY = 'some secret key'    # change
SQLALCHEMY_ECHO = False
BASEDIR = os.path.abspath(os.path.dirname(__file__))
SQLALCHEMY_DATABASE_URI = 'sqlite:///' + os.path.join(BASEDIR, 'db/app.db')
SQLALCHEMY_MIGRATE_REPO = os.path.join(BASEDIR, 'db/db_repository')
SQLALCHEMY_TRACK_MODIFICATIONS = True


# ================   User defined   ==========================

STATUS = ['Queued', 'Registered', 'Ongoing', 'Solved', 'Postponed', 'Blocked']
PRIORITY = ['Low', 'Medium', 'High']
ROLE = ['Admin', 'User']

TICKETS_PER_PAGE = 12
